//
//  AppDelegate.m
//  Localizer
//
//  Created by Aqeel Gunja on 7/20/15.
//  Copyright (c) 2015 Qeelio. All rights reserved.
//

#import "LOCAppDelegate.h"

@interface LOCAppDelegate ()

@end

@implementation LOCAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
    return YES;
}

@end
