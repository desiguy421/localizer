//
//  ViewController.m
//  Localizer
//
//  Created by Aqeel Gunja on 7/20/15.
//  Copyright (c) 2015 Qeelio. All rights reserved.
//

#import "LOCViewController.h"

typedef void(^LOCLanguageHandler)(NSString *language, NSString *languagePath);

@interface LOCLocalizedChunk : NSObject

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *value;
@property (nonatomic, copy) NSString *comment;

@end

@implementation LOCLocalizedChunk

- (NSUInteger)hash
{
    return self.key.hash ^ self.value.hash ^ self.comment.hash;
}

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[self class]])
    {
        LOCLocalizedChunk *otherChunk = object;
        return [self.key isEqualToString:otherChunk.key] && [self.value isEqualToString:otherChunk.value] && [self.comment isEqualToString:otherChunk.comment];
    }
    return NO;
}

@end

@interface LOCViewController ()

@property (nonatomic, strong) IBOutlet NSButton *selectProjectLocationButton;
@property (nonatomic, strong) IBOutlet NSTextField *projectLocationLabel;

@property (nonatomic, strong) IBOutlet NSButton *selectOutputFolderButton;
@property (nonatomic, strong) IBOutlet NSTextField *outputLocationLabel;

@property (nonatomic, strong) IBOutlet NSTextView *textView;
@property (nonatomic, strong) IBOutlet NSButton *checkBox;

@property (nonatomic, copy) NSString *projectLocation;
@property (nonatomic, copy) NSString *outputLocation;

@property (nonatomic, strong) NSMutableDictionary *localizedChunksDictionary;

@end



@implementation LOCViewController

@synthesize projectLocation = _projectLocation;
@synthesize outputLocation = _outputLocation;

- (void)setProjectLocation:(NSString *)projectLocation
{
    _projectLocation = projectLocation;
    
    [[NSUserDefaults standardUserDefaults] setObject:projectLocation forKey:@"LOCProjectLocation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)projectLocation
{
    if (!_projectLocation)
    {
        NSString *location = [[NSUserDefaults standardUserDefaults] objectForKey:@"LOCProjectLocation"];
        if (location)
        {
            _projectLocation = location;
        }
    }
    return _projectLocation;
}

- (void)setOutputLocation:(NSString *)outputLocation
{
    _outputLocation = outputLocation;
    
    [[NSUserDefaults standardUserDefaults] setObject:outputLocation forKey:@"LOCOutputLocation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)outputLocation
{
    if (!_outputLocation)
    {
        NSString *location = [[NSUserDefaults standardUserDefaults] objectForKey:@"LOCOutputLocation"];
        if (location)
        {
            _outputLocation = location;
        }
    }
    return _outputLocation;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if (self.projectLocation)
    {
        self.projectLocationLabel.stringValue = self.projectLocation;
    }

    if (self.outputLocation)
    {
        self.outputLocationLabel.stringValue = self.outputLocation;
    }
    
    self.localizedChunksDictionary = [NSMutableDictionary dictionary];
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction)selectProjectLocation:(id)sender
{
    NSString *location;
    
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    openPanel.canChooseFiles = NO;
    openPanel.canChooseDirectories = YES;
    openPanel.allowsMultipleSelection = NO;
    
    NSInteger clicked = [openPanel runModal];
    
    if (clicked == NSFileHandlingPanelOKButton)
    {
        location = [openPanel.URL path];
    }
    
    if (sender == self.selectProjectLocationButton)
    {
        self.projectLocationLabel.stringValue = location;
        self.projectLocation = location;
    }
    else if (sender == self.selectOutputFolderButton)
    {
        self.outputLocationLabel.stringValue = location;
        self.outputLocation = location;
    }
}

/* This is the value used by genstrings(1) when there is no comment in the code, so it can be generated too */
#define NO_COMMENT_PLACEHOLDER @" No comment provided by engineer. "

- (NSDictionary *)localizedChunksDictionaryForStringsFileText:(NSString *)text
{
    if (!text)
    {
        NSLog(@"blah");
    }
    
    NSMutableDictionary *existingChunksDictionary = [NSMutableDictionary dictionary];
    NSError *error = nil;
    NSString *pattern = @"(/[*](([*](?!/)|[^*])*)\\*/ *\n)? *\"((\\\\.|[^\"])*)\" *= *\"((\\\\.|[^\"])*)\" *;";
    NSRegularExpression *regularExpression = [[NSRegularExpression alloc] initWithPattern:pattern options:NSRegularExpressionDotMatchesLineSeparators error:&error];

    NSArray *matches = [regularExpression matchesInString:text options:0 range:NSMakeRange(0, [text length])];
    for (NSTextCheckingResult *match in matches)
    {
        NSRange commentRange = [match rangeAtIndex:2];
        NSString *comment = commentRange.length > 0 ? [text substringWithRange:commentRange] : NO_COMMENT_PLACEHOLDER;
        NSString *key = [text substringWithRange:[match rangeAtIndex:4]];
        NSString *value = [text substringWithRange:[match rangeAtIndex:6]];
        
        LOCLocalizedChunk *localizedChunk = [[LOCLocalizedChunk alloc] init];
        localizedChunk.key = key;
        localizedChunk.value = value;
        localizedChunk.comment = comment;
        [existingChunksDictionary setObject:localizedChunk forKey:key];
    }
    return [existingChunksDictionary copy];
}

- (IBAction)localize:(id)sender
{
    if (!self.outputLocation || !self.projectLocation)
    {
        [self appendMessageToTextView:@"Please select a source location and an output location."];
        return;
    }
    [self localizeChunks];
}

- (void)reconcileExistingChunksInLanguage:(NSString *)language withNewChunks:(NSDictionary *)newChunksDictionary
{
    NSMutableDictionary *existingChunksDictionary = [[self.localizedChunksDictionary objectForKey:language] mutableCopy];
    for (NSString *newKey in newChunksDictionary)
    {
        LOCLocalizedChunk *existing = [existingChunksDictionary objectForKey:newKey];
        LOCLocalizedChunk *newChunk = newChunksDictionary[newKey];
        if (!existing)
        {
            [existingChunksDictionary setObject:newChunk forKey:newKey];
        }
        else if (newChunk.comment.length && ![newChunk.comment isEqualToString:NO_COMMENT_PLACEHOLDER])
        {
            existing.comment = newChunk.comment;
        }
    }
    
    if (self.checkBox.state == NSOnState)
    {
        NSMutableSet *existingKeys = [NSMutableSet setWithArray:[existingChunksDictionary allKeys]];
        NSMutableSet *newKeys = [NSMutableSet setWithArray:[newChunksDictionary allKeys]];
        [existingKeys minusSet:newKeys];
        for (NSString *staleKey in existingKeys)
        {
            [existingChunksDictionary removeObjectForKey:staleKey];
        }
    }
    
    [self.localizedChunksDictionary setObject:[existingChunksDictionary copy] forKey:language];
}

- (void)enumerateOverLanguageDirectoriesUsingHandler:(LOCLanguageHandler)handler
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:self.outputLocation error:NULL];
    for (NSString *fileContent in contents)
    {
        NSString *fullPath = [self.outputLocation stringByAppendingPathComponent:fileContent];
        BOOL isDirectory;
        [fileManager fileExistsAtPath:fullPath isDirectory:&isDirectory];
        if (isDirectory)
        {
            if ([fileContent hasSuffix:@".lproj"])
            {
                NSRange lprojRange = [fileContent rangeOfString:@".lproj"];
                NSString *language = [fileContent substringToIndex:lprojRange.location];
                NSString *languagePath = [self.outputLocation stringByAppendingPathComponent:fileContent];
                handler(language, languagePath);
            }
        }
    }
}

- (void)localizeChunks
{
    [self enumerateOverLanguageDirectoriesUsingHandler:^(NSString *language, NSString *languagePath)
    {
        [self gatherExistingLocalizedChunksForLanguage:language atLanguagePath:languagePath];
        [self generateNewLocalizedChunksForLanguage:language atLanguagePath:languagePath];
        [self createNewStringsFileFromExistingChunksForLanguage:language atLanguagePath:languagePath];
    }];
}

- (void)gatherExistingLocalizedChunksForLanguage:(NSString *)language atLanguagePath:(NSString *)languagePath
{
    NSString *localizableStringsPath = [languagePath stringByAppendingPathComponent:@"Localizable.strings"];
    NSString *localizableStringsBackupPath = [languagePath stringByAppendingPathComponent:@"Localizable.strings-backup"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *moveError = nil;
    [fileManager moveItemAtPath:localizableStringsPath toPath:localizableStringsBackupPath error:&moveError];
    if (moveError)
    {
        NSLog(@"whoops: %@", moveError);
    }
    NSError *error = nil;
    NSString *stringsText = [[NSString alloc] initWithContentsOfFile:localizableStringsBackupPath encoding:NSUTF8StringEncoding error:&error];
    if (error)
    {
        NSString *errorString = [NSString stringWithFormat:@"Error reading existing strings file for language: %@ \n Error: %@",language, error];
        [self appendMessageToTextView:errorString];
    }
    NSDictionary *chunks = [self localizedChunksDictionaryForStringsFileText:stringsText];
    [self.localizedChunksDictionary setObject:chunks forKey:language];
    
}

- (void)generateNewLocalizedChunksForLanguage:(NSString *)language atLanguagePath:(NSString *)languagePath
{
    [self generateNewStringsToOutputLocation:languagePath];
    
    NSString *localizableStringsPath = [languagePath stringByAppendingPathComponent:@"Localizable.strings"];
    
    NSError *error = nil;
    NSString *stringsText = [[NSString alloc] initWithContentsOfFile:localizableStringsPath encoding:NSUTF16StringEncoding error:&error];
    if (error)
    {
        NSString *errorString = [NSString stringWithFormat:@"Error reading newly generated strings file for language: %@ \n Error: %@",language, error];
        [self appendMessageToTextView:errorString];
    }
    NSDictionary *chunks = [self localizedChunksDictionaryForStringsFileText:stringsText];
    [self reconcileExistingChunksInLanguage:language withNewChunks:chunks];
}

- (void)createNewStringsFileFromExistingChunksForLanguage:(NSString *)language atLanguagePath:(NSString *)languagePath
{
    NSString *localizableStringsPath = [languagePath stringByAppendingPathComponent:@"Localizable.strings"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:localizableStringsPath error:NULL];

    NSMutableString *stringsFileText = [NSMutableString string];
    
    NSArray *localizedChunks = [[[self.localizedChunksDictionary objectForKey:language] allValues] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2)
                                {
                                    LOCLocalizedChunk *chunk1 = obj1;
                                    LOCLocalizedChunk *chunk2 = obj2;
                                    return [chunk1.key compare:chunk2.key];
                                }];
    
    for (LOCLocalizedChunk *chunk in localizedChunks)
    {
        [stringsFileText appendFormat:@"/*%@*/\n\"%@\" = \"%@\";\n\n",chunk.comment, chunk.key, chunk.value];
    }
    [stringsFileText appendString:@"\n"];
    [stringsFileText writeToFile:localizableStringsPath atomically:YES encoding:NSUTF8StringEncoding error:NULL];
    NSString *message = [NSString stringWithFormat:@"Generated strings file for %@", language];
    [self appendMessageToTextView:message];
    
    NSString *localizableStringsBackupPath = [languagePath stringByAppendingPathComponent:@"Localizable.strings-backup"];
    [fileManager removeItemAtPath:localizableStringsBackupPath error:NULL];
}

// Old Methods

- (void)gatherExistingLocalizedChunks
{
    [self enumerateOverLanguageDirectoriesUsingHandler:^(NSString *language, NSString *languagePath)
    {
        [self gatherExistingLocalizedChunksForLanguage:language atLanguagePath:languagePath];
    }];
}

- (void)generateNewLocalizedChunks
{
    [self enumerateOverLanguageDirectoriesUsingHandler:^(NSString *language, NSString *languagePath)
    {
        [self generateNewLocalizedChunksForLanguage:language atLanguagePath:languagePath];
    }];
}

- (void)createNewStringsFileFromExistingChunks
{
    [self enumerateOverLanguageDirectoriesUsingHandler:^(NSString *language, NSString *languagePath)
    {
        [self createNewStringsFileFromExistingChunksForLanguage:language atLanguagePath:languagePath];
    }];
}

- (void)appendMessageToTextView:(NSString *)message
{
    NSString *messageToAppend = [NSString stringWithFormat:@"\n%@\n", message];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:messageToAppend];
    [self.textView.textStorage appendAttributedString:attributedString];
    [self.textView scrollRangeToVisible:NSMakeRange([[self.textView string] length], 0)];
}

- (void)generateNewStringsToOutputLocation:(NSString *)outputLocation
{
    NSTask *task = [[NSTask alloc] init];
    
    NSString *commandToRun = @"find -s %@ -regex \".*\\.m$\" -print0 | xargs -0 -L 1000000 genstrings -q -o %@";
    
    commandToRun = [NSString stringWithFormat:commandToRun, self.projectLocation, outputLocation];
    
    [task setLaunchPath:@"/bin/sh"];
    
    NSArray *arguments = [NSArray arrayWithObjects:
                          @"-c" ,
                          [NSString stringWithFormat:@"%@", commandToRun],
                          nil];
    [self appendMessageToTextView:commandToRun];
    [task setArguments:arguments];
    
    NSPipe *pipe = [NSPipe pipe];
    [task setStandardOutput:pipe];
    
    [task launch];
    [task waitUntilExit];
}

@end
