//
//  main.m
//  Localizer
//
//  Created by Aqeel Gunja on 7/20/15.
//  Copyright (c) 2015 Qeelio. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
